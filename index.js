const Discord = require('discord.js')
client = new Discord.Client(),

config = require('./config.json') //J'appelle le fichier de configuration

client.login(config.tokenBot) //Connexion au bot avec le token du bot de ./config.json
client.commands = new Discord.Collection() //Stocké les commandes dans le client

//Gestion info du bot
client.once('ready', () => {
    console.info(`${client.user.tag} launched`);
    client.user.setActivity("Être un gentil robot");
})

const fs = require("fs") //Module d'accès au fichier

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js')); //Stock dans un tableau tout les fichiers dans ./commands qui finissent par .js

for (const file of commandFiles){ //Pour chaque fichier de commandes de tout les fichiers de commandes
    const command = require(`./commands/${file}`) //command = un fichier de commandes dont on récupère le contenu + on va chercher le fichier $file
    client.commands.set(command.name, command) //commandes du client défini et set avec les args "name" et "command"
}



//Analyse des messages
client.on('message', message => {


    if (message.type !== 'DEFAULT' || message.author.bot) return; //Si le message n'est pas "Par défaut" et qu'il provient d'un bot NOTHING HAPPEN
    const args = message.content.split(/ +/) //Récupère l'argument dans le message 
    //On récupère la commaned et la vérifie
 
    const commandName = args.shift().toLowerCase() //Fait fonctionnner les commandes même en majuscules
    if (!commandName.startsWith(config.prefix)) return // Si la commande ne commence pas par le préfix et bah NIQUE

    const command = client.commands.get(commandName.slice(config.prefix.length)) //La "commandS" est rechercher en fonction du commandName duquel on a retiré le préfix
    if (!command)
    {message.reply("La commande n'est pas compatible avec mon programme"); return} // Si c'est une commande qui existe pas FUCK IT

    //On execute la commande
    try{
        command.execute(message, args)
    }
    catch (error){
        console.error(error);
        message.reply("Un problème dans mes circuits est survenu"); //Réponse en cas d'erreur
    }
})